grammar calc;

/** The start rule; begin parsing here. */
prog: stat+ ;

stat: expr NEWLINE ;

expr: expr ('*'|'/') expr
    | expr ('+'|'-') expr
    | INT
    | '(' expr ')' ;


INT : [0-9]+ ; // match integers

NEWLINE: '\n' ; // return newlines to pa

//WS : [ \t]+ -> skip ; // toss out whitespace